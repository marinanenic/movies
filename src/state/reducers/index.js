import {combineReducers} from "redux";
import auth from "./authReducer";
import comments from "./commentsReducer";

const rootReducer = combineReducers({
  auth,
  comments,
});

export default rootReducer;
