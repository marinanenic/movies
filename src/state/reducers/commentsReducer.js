const initialState = {
  loading: false,
  data: []
};
import {types} from "../actions/commentsActions";

const reducer = (state = initialState, action) => {
  const {type, payload} = action;

  switch (type) {
    case types.GET_MOVIE_COMMENTS:
      return {
        ...state,
        loading: true
      };
    case types.GET_MOVIE_COMMENTS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: payload
      };
    default:
      return state;
  }
};

export default reducer;
