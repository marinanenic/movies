import {types} from "../actions/authActions";

const initialState = {
  user: {
    uid: "",
    email: "",
    display_name: ""
  },
  isLogging: false,
  error: ""
};

const reducer = (state = initialState, action) => {
  const {type, payload} = action;

  switch (type) {
    case types.LOGIN_USER:
      return {
        ...state,
        isLogging: true,
        error: ""
      };
    case types.LOGIN_USER_SUCCESS:
      return {
        ...state,
        user: payload,
        isLogging: false
      };
    case types.LOGIN_USER_FAIL:
      return {
        ...state,
        isLogging: false,
        error: payload
      };
    case types.SET_CURRENT_USER:
      return {
        ...state,
        user: payload
      };
    default:
      return state;
  }
};

export default reducer;
