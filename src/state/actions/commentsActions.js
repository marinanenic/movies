import firebase from "firebase";

export const types = {
  GET_MOVIE_COMMENTS: "GET_MOVIE_COMMENTS",
  GET_MOVIE_COMMENTS_FAIL: "GET_MOVIE_COMMENTS_FAIL",
  GET_MOVIE_COMMENTS_SUCCESS: "GET_MOVIE_COMMENTS_SUCCESS",
  ADD_COMMENT: "ADD_COMMENT",
  ADD_COMMENT_FAIL: "ADD_COMMENT_FAIL",
  ADD_COMMENT_SUCCESS: "ADD_COMMENT_SUCCESS",
};

export const getMovieComments = (movieId) => {
  return (dispatch) => {
    dispatch({type: types.GET_MOVIE_COMMENTS});
    firebase.database().ref(`movies/${movieId}/comments`)
      .on("value", snapshot => {
          const data = snapshot.val() ? Object.entries(snapshot.val())
            .map(([id, value]) => ({id, ...value})).reverse() : [];
          dispatch({type: types.GET_MOVIE_COMMENTS_SUCCESS, payload: data})
        }
      );
  }
};

export const addComment = ({movieId, comment}) => {
  return (dispatch) => {
    dispatch({type: types.ADD_COMMENT});

    firebase.database().ref(`movies/${movieId}/comments`)
      .push(comment)
      .then(() => {
        dispatch({type: types.ADD_COMMENT_SUCCESS})
      })
      .catch(error => {
        dispatch({type: types.ADD_COMMENT_FAIL, payload: error})
      })
  }
};

export const removeCommentListener = (movieId) => {
  firebase.database().ref(`movies/${movieId}/comments`).off();
};
