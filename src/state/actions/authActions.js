import firebase from 'firebase';
import {Actions} from 'react-native-router-flux';

export const types = {
  LOGIN_USER_SUCCESS: "LOGIN_USER_SUCCESS",
  LOGIN_USER_FAIL: "LOGIN_USER_FAIL",
  LOGIN_USER: "LOGIN_USER",
  SET_CURRENT_USER: "GET_CURRENT_USER"
};

export const loginUser = ({email, password}) => {
  return (dispatch) => {
    dispatch({type: types.LOGIN_USER});

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => {
        dispatch({
          type: types.LOGIN_USER_SUCCESS,
          payload: user
        });
      }).catch((error) => {
      dispatch({type: types.LOGIN_USER_FAIL, payload: error.message});
    });
  };
};

export const registerUser = ({email, password}) => {
  return (dispatch) => {
    dispatch({type: types.LOGIN_USER});

    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(user => {
        dispatch({
          type: types.LOGIN_USER_SUCCESS,
          payload: user
        });
      })
      .catch((error) => {
        dispatch({type: types.LOGIN_USER_FAIL, payload: error.message});
      });
  }
};

export const getCurrentUser = () => {
  return (dispatch) => {
    firebase.auth().onAuthStateChanged(user => {
        dispatch({
          type: types.SET_CURRENT_USER,
          payload: user
        });
        if (user) {
          Actions.main();
        } else {
          Actions.auth();
        }
      }
    )
  }
};
