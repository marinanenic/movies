import React from "react";
import {Text, TextInput, View} from "react-native";

const Input = ({label, value, placeholder, onChange, secure, multiline, icon}) => {
  const {labelStyle, inputStyle, inputContainerStyle} = style;
  return (
    <View>
      {label && <Text style={labelStyle}>{label}</Text>}
      <View style={inputContainerStyle}>
        <TextInput
          value={value}
          placeholder={placeholder}
          onChangeText={onChange}
          secureTextEntry={secure}
          style={inputStyle}
          multiline={multiline}
        />
        {icon}
      </View>
    </View>
  )
};

const style = {
  labelStyle: {
    paddingLeft: 15,
    paddingRight: 15,
    fontSize: 12,
    lineHeight: 15,
  },
  inputContainerStyle: {
    flexDirection: "row",
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "gray",
    marginHorizontal: 15,
    marginBottom: 20,
    marginTop: 10
  },
  inputStyle: {
    flex:1,
    fontSize: 18,
    lineHeight: 23,
    maxHeight: 50
  }
};

export default Input;
