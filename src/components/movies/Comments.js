import React, {Component} from "react";
import {connect} from "react-redux";
import {FlatList, StyleSheet, ActivityIndicator, View, TouchableOpacity} from "react-native";
import {Ionicons} from "@expo/vector-icons/build/Icons";
import {getMovieComments, addComment, removeCommentListener} from "../../state/actions/commentsActions";
import Comment from "./Comment";
import Input from "../common/Input";

class Comments extends Component {

  state = {
    text: ""
  };

  componentWillMount() {
    this.props.getMovieComments(this.props.movieId);
  }

  submit = () => {
    const {movieId, addComment, user: {email}} = this.props;
    const {text} = this.state;
    addComment({movieId, comment: {email, text}});
    this.setState({text: ""})
  };

  renderComments = () => {
    return this.props.loading ?
      <ActivityIndicator style={style.loader} size="large" color="#0000ff"/>
      :
        <FlatList
          inverted
          data={this.props.data}
          renderItem={({item}) => <Comment comment={item}/>}
          keyExtractor={comment => comment.id}
          style={style.listStyle}
        />
  };

  render() {
    return (
      <View style={style.container}>
        {this.renderComments()}
        <Input
          placeholder={"Enter your comment"}
          value={this.state.text}
          onChange={text => this.setState({text})}
          multiline
          icon={
            <TouchableOpacity onPress={() => this.submit()}>
              <Ionicons
                name={"md-send"}
                size={18}
                style={style.iconStyle}
              />
            </TouchableOpacity>
          }
        />
      </View>
    );
  }

  componentWillUnmount() {
    removeCommentListener(this.props.movieId);
  }
}

const mapStateToProps = ({auth: {user}, comments: {loading, data}}) => ({
  loading,
  data,
  user
});

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-end",
    backgroundColor: "#F8F8F8",
  },
  iconStyle: {
    paddingLeft: 15,
    paddingRight: 15
  },
  loader: {
    flex: 1,
    justifyContent: 'center',
  }
});

export default connect(mapStateToProps, {getMovieComments, addComment})(Comments);
