import React, {Component} from "react";
import {FlatList, StyleSheet} from 'react-native';
import {movies} from "../../utils/moviesList";
import MovieCard from "./MovieCard";

class Movies extends Component {
  render() {
    return (
      <FlatList
        data={movies}
        renderItem={({item}) => <MovieCard movie={item}/>}
        keyExtractor={movie => movie.rank}
        style={style.listStyle}
      />
    );
  }
}

const style = StyleSheet.create({
  listStyle: {
    backgroundColor: "#F8F8F8"
  }
});

export default Movies;
