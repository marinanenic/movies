import React, {PureComponent} from "react";
import {StyleSheet, View, Text} from "react-native";

class Comment extends PureComponent {

  render() {
    const {containerStyle, authorStyle} = style;
    const {email, text} = this.props.comment;

    return (
      <View style={containerStyle}>
        <Text style={authorStyle}>{email}</Text>
        <Text>{text}</Text>
      </View>
    )
  }
}

const style = StyleSheet.create({
  containerStyle: {
    margin: 5,
    padding: 10,
    backgroundColor: "#fff",
  },
  authorStyle: {
    fontSize: 10,
    fontWeight: "bold",
    flex:1
  }
});

export default Comment;
