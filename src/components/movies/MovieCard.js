import React, {PureComponent} from "react";
import {View, Text, StyleSheet, TouchableOpacity} from "react-native";
import {Actions} from "react-native-router-flux";
import {Feather} from '@expo/vector-icons';

class MovieCard extends PureComponent {

  render() {
    const {rank, title, year, rating, runtime, genre, actors} = this.props.movie;
    const {containerStyle, titleStyle, basicInfoStyle, flexStyle, iconStyle} = style;
    return (
      <View style={containerStyle}>
        <View style={flexStyle}>
          <Text style={titleStyle}>{`${rank}. ${title}`}</Text>
          <TouchableOpacity onPress={() => Actions.comments({movieId:rank, title})}>
            <Feather name={"message-square"} size={18} style={iconStyle}/>
          </TouchableOpacity>
        </View>
        <View style={{...basicInfoStyle, ...flexStyle}}>
          <Text>{`Rating: ${rating}`}</Text>
          <Text>{`Year: ${year}`}</Text>
          <Text>{`Duration: ${runtime}`}</Text>
        </View>
        <Text>{`Genre: ${genre.join(", ")}`}</Text>
        <Text>{`Cast: ${actors.join(", ")}`}</Text>
      </View>
    )
  }
}

const style = StyleSheet.create({
  containerStyle: {
    margin: 5,
    padding: 10,
    backgroundColor: "#fff"
  },
  titleStyle: {
    fontSize: 18,
    fontWeight: "bold",
    flex:1
  },
  flexStyle: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  basicInfoStyle: {
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 5,
    marginTop: 5,
    borderBottomWidth: 1,
    borderBottomColor: "#F8F8F8",
    borderTopWidth: 1,
    borderTopColor: "#F8F8F8"
  },
  iconStyle: {
    paddingLeft: 15,
    paddingRight: 15
  }
});

export default MovieCard;
