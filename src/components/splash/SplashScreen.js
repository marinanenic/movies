import React from "react";
import {Image, View, StyleSheet} from "react-native";
import SplashImage from "../../../assets/splash.png";

const SplashScreen = () => {
  const {container, backgroundImage} = styles;
  return (
    <View style={container}>
      <Image source={SplashImage} style={backgroundImage}/>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  backgroundImage: {
    width: "100%",
  }
});

export default SplashScreen;
