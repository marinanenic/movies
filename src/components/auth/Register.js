import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Text, ActivityIndicator} from 'react-native';
import {Actions} from "react-native-router-flux";
import {registerUser} from "../../state/actions/authActions";
import EmailPasswordForm from "./EmailPasswordForm";

class Register extends Component {
  renderButton = ({email, password}) => {
    return this.props.isLogging ?
      <ActivityIndicator size="large" color="#5494f9"/>
      :
      <Button
        title={"Register"}
        onPress={() => this.props.registerUser({email, password})}
        color={"#5494f9"}
      />
  };

  render() {
    return (
      <EmailPasswordForm
        renderButton={this.renderButton}
        titleText={"Register"}
        error={this.props.error}
      >
        <Text onPress={Actions.login}>
          Already have an account? Log in
        </Text>
      </EmailPasswordForm>
    )
  }
}

const mapStateToProps = ({auth: {isLogging, error}}) => ({
  isLogging,
  error
});

export default connect(mapStateToProps, {registerUser})(Register);

