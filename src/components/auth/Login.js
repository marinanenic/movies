import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Text, ActivityIndicator} from 'react-native';
import {Actions} from "react-native-router-flux";
import EmailPasswordForm from "./EmailPasswordForm";
import {loginUser} from "../../state/actions/authActions";

class Login extends Component {
  renderButton = ({email, password}) => {
    return this.props.isLogging ?
      <ActivityIndicator size="large" color="#5494f9"/>
      :
      <Button
        title={"Login"}
        onPress={() => this.props.loginUser({email, password})}
        color={"#5494f9"}
      />
  };

  render() {
    return (
      <EmailPasswordForm
        renderButton={this.renderButton}
        titleText={"Please log in!"}
        error={this.props.error}
      >
        <Text onPress={Actions.register}>
          Don't have an account? Sign up
        </Text>
      </EmailPasswordForm>
    )
  }
}

const mapStateToProps = ({auth: {isLogging, error}}) => ({
  isLogging,
  error
});

export default connect(mapStateToProps, {loginUser})(Login);
