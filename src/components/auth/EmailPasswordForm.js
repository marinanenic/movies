import React, {Component} from 'react';
import {View, Text} from 'react-native';
import Input from "../common/Input";

class EmailPasswordForm extends Component {

  state = {
    email: '',
    password: ''
  };

  render() {
    const {email, password} = this.state;
    const {formStyle, headingStyle, registerStyle, errorStyle} = style;
    return (
      <View style={formStyle}>
        <Text style={headingStyle}>{this.props.titleText}</Text>
        <Input
          label={"Email"}
          value={email}
          placeholder={'Example@test.com'}
          onChange={email => this.setState({email})}
        />
        <Input
          label={"Password"}
          value={password}
          placeholder={"Enter password"}
          onChange={password => this.setState({password})}
          secure={true}
        />
        <Text style={errorStyle}>{this.props.error}</Text>
        {this.props.renderButton({email, password})}
        <Text style={registerStyle}>
          {this.props.children}
        </Text>
      </View>
    )
  }
}

const style = {
  headingStyle: {
    fontSize: 32,
    marginBottom: 50,
    textAlign: "center"
  },
  formStyle: {
    flex: 1,
    justifyContent: 'center',
    marginRight: 40,
    marginLeft: 40
  },
  registerStyle: {
    fontSize: 14,
    marginTop: 20,
    color: "#5494f9",
    textAlign: "center"
  },
  errorStyle: {
    textAlign: "center",
    color: "red",
    fontSize: 14,
    minHeight: 16
  }
};

export default EmailPasswordForm;
