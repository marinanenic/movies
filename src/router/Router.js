import React from "react";
import {KeyboardAvoidingView} from "react-native";
import {Scene, Router as FluxRouter, ActionConst} from "react-native-router-flux";
import firebase from "firebase";
import Login from "../components/auth/Login";
import Register from "../components/auth/Register";
import Movies from "../components/movies/Movies";
import Comments from "../components/movies/Comments";
import SplashScreen from "../components/splash/SplashScreen";

const Router = () => (
  <KeyboardAvoidingView style={styles.sceneStyle} behavior="padding">
    <FluxRouter navigationBarStyle={styles.navBar} titleStyle={styles.navTitle} sceneStyle={styles.sceneStyle}>
      <Scene key={"root"} hideNavBar type={ActionConst.RESET}>
        <Scene key={"splash"} hideNavBar component={SplashScreen}/>
        <Scene key={"auth"} hideNavBar>
          <Scene key={"login"} component={Login}/>
          <Scene key={"register"} component={Register}/>
        </Scene>
        <Scene key={"main"} type={ActionConst.RESET}>
          <Scene
            key={"movies"}
            component={Movies}
            title={"Movies"}
            onRight={() => firebase.auth().signOut()}
            rightTitle={"Log out"}
          />
          <Scene key={"comments"} component={Comments} title={"Comments"}/>
        </Scene>
      </Scene>
    </FluxRouter>
  </KeyboardAvoidingView>
);

const styles = {
  navBar: {
    backgroundColor: "#F8F8F8"
  },
  navTitle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  sceneStyle: {
    backgroundColor: '#fff',
    flex: 1
  }
};

export default Router;
