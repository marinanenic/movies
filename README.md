# movies

File structure: 

- src

    - components - ui components organized in folders (commons and separate folder for each app section)
    - router - navigation setup
    - state - redux actions and reducers
    - utils - utility files

Project implementation: 

- for navigation I have used react native router flux
- I used firebase’s onAuthStateChanged to keep track of logged user and based on that show auth or movie screens
- I used redux for comments, it wasn’t really necessary since the app is simple but it is nice to separate actions and functionalities from visual parts of application

Improvements: 

- move firebase API call in separate util file

