import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from "react-redux";
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import Router from './src/router/Router';
import rootReducer from './src/state/reducers'
import {getCurrentUser} from "./src/state/actions/authActions";
import firebaseConfig from "./src/utils/firebaseConfig";

const store = createStore(rootReducer, {}, applyMiddleware(ReduxThunk));

export default class App extends React.Component {
  componentWillMount() {
    firebase.initializeApp(firebaseConfig);
    store.dispatch(getCurrentUser());
  }

  render() {
    return (
      <Provider store={store}>
        <Router/>
      </Provider>
    );
  }
}

